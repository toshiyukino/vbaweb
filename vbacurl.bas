Public Function vbacurl(str_option As String, str_url As String) As Object
  Dim RegExp As Object  'VBScript_RegExp_55.RegExp
  Dim Match As Object   'VBScript_RegExp_55.Match
  Dim Matches As Object 'VBScript_RegExp_55.MatchCollection
  Dim xhr As Object     'MSXML2.ServerXMLHTTP60
  Dim dict As Object    'Scripting.Dictionary
  Dim ua As String
  Dim strRequestParam As String
  Dim isResponseHeader As String
  Dim ProxyUser As String

  'dictionary
  Set dict = CreateObject("scripting.dictionary")
  dict("status") = ""
  dict("statustext") = ""
  dict("header") = ""
  dict("data") = ""
  
  'On Error GoTo ErrorHandler
  
  'RegExp Setting
  Set RegExp = CreateObject("VBScript.RegExp")
  RegExp.IgnoreCase = False '大文字小文字を区別する
  RegExp.Global = True
  'regExp.Pattern = ""
  Set xhr = CreateObject("Msxml2.ServerXMLHTTP.6.0")

  'データ
  RegExp.Pattern = "-d\s(\S*)"
  Set Matches = RegExp.Execute(str_option)
  If Matches.Count > 0 Then
    strRequestParam = Matches(0).SubMatches(0)
  End If
  Debug.Print strRequestParam

  'ヘッダーを返すか
  RegExp.Pattern = "-i"
  Set Matches = RegExp.Execute(str_option)
  If Matches.Count > 0 Then
    isResponseHeader = Matches(0).Value
  End If
  Debug.Print isResponseHeader
  
  'UA設定
  RegExp.Pattern = "-A\s(\S*)"
  Set Matches = RegExp.Execute(str_option)
  If Matches.Count > 0 Then
    ua = Matches(0).SubMatches(0)
  Else
    ua = "Mozilla/4.0"
  End If
  Debug.Print ua

  
  'プロキシ設定
  RegExp.Pattern = "-x\s(\S*)"
  Set Matches = RegExp.Execute(str_option)
  If Matches.Count > 0 Then
    xhr.SetProxy 2, Matches(0).SubMatches(0)  'SXH_PROXY_SET_PROXY=2
  End If
  
  'プロキシ認証設定
  RegExp.Pattern = "-U\s(\S*)"
  Set Matches = RegExp.Execute(str_option)
  If Matches.Count > 0 Then
    ProxyUser = Matches(0).SubMatches(0)
  End If
  
  
  'オブジェクトを開く
  Call xhr.Open("GET", str_url & "?" & strRequestParam, False) '同期処理
  
  'プロキシ認証
  If Len(ProxyUser) > 0 Then
    If InStr(1, ProxyUser, ":") > 0 Then
      xhr.setProxyCredentials Split(ProxyUser, ":")(0), Split(ProxyUser, ":")(1)
    End If
  End If
  
  'リクエストヘッダーのセット
'  Call xhr.setRequestHeader("User-Agent", ua)
'  Call xhr.setRequestHeader("Pragma", "no-cache")
'  Call xhr.setRequestHeader("Cache-Control", "Private")
'  Call xhr.setRequestHeader("Expires", "-1")
  
  '送信
  Call xhr.send
  dict("status") = xhr.Status
  dict("statustext") = xhr.statusText
  
  If isResponseHeader = "-i" Then
    dict("header") = xhr.getAllResponseHeaders
    dict("data") = xhr.responseText
  Else
    dict("data") = xhr.responseBody
  End If
  
  '正常終了
  Set vbacurl = dict
  Set dict = Nothing
  Exit Function
'エラー
ErrorHandler:
  Set vbacurl = dict
  Set dict = Nothing
End Function

